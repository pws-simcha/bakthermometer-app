package me.simcha.bakthermometer;

import org.junit.Assert;
import org.junit.Test;

import me.simcha.bakthermometer.packet.Packet;
import me.simcha.bakthermometer.packet.PacketParser;
import me.simcha.bakthermometer.packet.packets.clientside.ChangeAlarmAtPacket;

public class PacketTest {

    @Test
    public void testParsePacket() {
        ChangeAlarmAtPacket packet = new ChangeAlarmAtPacket(54656);
        String json = packet.toJson();

        Packet parsedPacket = new PacketParser().parsePacket(json);
        Assert.assertEquals(json, parsedPacket.toJson());
    }
}
