package me.simcha.bakthermometer.broadcast.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import me.simcha.bakthermometer.Constants;
import me.simcha.bakthermometer.packet.IPacketListener;
import me.simcha.bakthermometer.packet.Packet;
import me.simcha.bakthermometer.packet.PacketParser;

public class PacketBroadcastReceiver extends BroadcastReceiver {

	private final static PacketParser PACKET_PARSER = new PacketParser();

	private final IPacketListener packetListener;

	public PacketBroadcastReceiver(IPacketListener packetListener) {
		this.packetListener = packetListener;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent != null) {
			Packet packet = PACKET_PARSER.parsePacket(intent.getStringExtra(Constants.PacketBroadcast.PACKET_FIELD));
			if (packet == null) {
				// Received data isn't a valid packet. Ignore it.
				return;
			}
			packetListener.onReceive(packet);
		}
	}

	public void register(Context context) {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Constants.PacketBroadcast.ACTION);
		context.registerReceiver(this, filter);
	}
}
