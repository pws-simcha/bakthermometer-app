package me.simcha.bakthermometer.broadcast.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import me.simcha.bakthermometer.Constants;

public class ExceptionBroadcastReceiver extends BroadcastReceiver {

	private final IExceptionListener exceptionListener;

	public ExceptionBroadcastReceiver(IExceptionListener exceptionListener) {
		this.exceptionListener = exceptionListener;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent != null) {
			Exception ex = (Exception) intent.getSerializableExtra(Constants.ExceptionBroadcast.EXCEPTION_FIELD);
			exceptionListener.onReceive(ex);
		}
	}

	public void register(Context context) {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Constants.ExceptionBroadcast.ACTION);
		context.registerReceiver(this, filter);
	}

	public interface IExceptionListener {

		void onReceive(Exception ex);
	}
}
