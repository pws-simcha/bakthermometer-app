package me.simcha.bakthermometer.broadcast.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import me.simcha.bakthermometer.Constants;
import me.simcha.bakthermometer.model.MeasurementInfo;

public class MeasurementInfoBroadcastReceiver extends BroadcastReceiver {

	private final IMeasurementInfoListener measurementInfoListener;

	public MeasurementInfoBroadcastReceiver(IMeasurementInfoListener measurementInfoListener) {
		this.measurementInfoListener = measurementInfoListener;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent != null) {
			MeasurementInfo info = intent.getParcelableExtra(Constants.MeasurementInfoBroadcast.INFO_FIELD);
			measurementInfoListener.onReceive(info);
		}
	}

	public void register(Context context) {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Constants.MeasurementInfoBroadcast.ACTION);
		context.registerReceiver(this, filter);
	}

	public interface IMeasurementInfoListener {

		void onReceive(MeasurementInfo info);
	}
}
