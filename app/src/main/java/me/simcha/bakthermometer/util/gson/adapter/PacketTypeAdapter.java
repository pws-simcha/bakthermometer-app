package me.simcha.bakthermometer.util.gson.adapter;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import me.simcha.bakthermometer.packet.Packet;
import me.simcha.bakthermometer.util.gson.GsonFactory;

public class PacketTypeAdapter implements JsonDeserializer<Packet> {

	@Override
	public Packet deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		int id = json.getAsJsonObject().get("id").getAsInt();
		if (id < 0 || id >= Packet.Type.values().length) {
			throw new JsonParseException("There is no packet type with id " + id);
		}

		Class<? extends Packet> packetClass = Packet.Type.values()[id].getPacketClass();
		return GsonFactory.GSON.fromJson(json, packetClass);
	}
}
