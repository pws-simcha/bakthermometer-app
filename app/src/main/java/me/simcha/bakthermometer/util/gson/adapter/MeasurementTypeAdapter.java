package me.simcha.bakthermometer.util.gson.adapter;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;

import java.lang.reflect.Type;

import me.simcha.bakthermometer.packet.packets.serverside.NewDataPacket;
import me.simcha.bakthermometer.util.gson.TypeAdapter;

public class MeasurementTypeAdapter implements TypeAdapter<NewDataPacket.Measurement> {

    @Override
    public NewDataPacket.Measurement deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray array = json.getAsJsonArray();
        return new NewDataPacket.Measurement(array.get(0).getAsDouble(), array.get(1).getAsDouble());
    }

    @Override
    public JsonElement serialize(NewDataPacket.Measurement src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray array = new JsonArray();
        array.add(src.getTimeSinceStart());
        array.add(src.getTemperature());
        return array;
    }
}
