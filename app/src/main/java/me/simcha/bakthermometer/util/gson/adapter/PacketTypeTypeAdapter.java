package me.simcha.bakthermometer.util.gson.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;

import java.lang.reflect.Type;

import me.simcha.bakthermometer.packet.Packet;
import me.simcha.bakthermometer.util.gson.TypeAdapter;

public class PacketTypeTypeAdapter implements TypeAdapter<Packet.Type> {

    @Override
    public Packet.Type deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        int id = json.getAsInt();
        if (id < 0 || id >= Packet.Type.values().length) {
            throw new JsonParseException("There is no packet type with id " + id);
        }
        return Packet.Type.values()[id];
    }

    @Override
    public JsonElement serialize(Packet.Type src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.ordinal());
    }
}
