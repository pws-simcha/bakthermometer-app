package me.simcha.bakthermometer.util.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import me.simcha.bakthermometer.packet.Packet;
import me.simcha.bakthermometer.packet.packets.serverside.NewDataPacket;
import me.simcha.bakthermometer.util.gson.adapter.MeasurementTypeAdapter;
import me.simcha.bakthermometer.util.gson.adapter.PacketTypeAdapter;
import me.simcha.bakthermometer.util.gson.adapter.PacketTypeTypeAdapter;

public class GsonFactory {

	public static final Gson GSON = new GsonBuilder() //
			.registerTypeAdapter(Packet.class, new PacketTypeAdapter()) //
			.registerTypeAdapter(Packet.Type.class, new PacketTypeTypeAdapter()) //
			.registerTypeAdapter(NewDataPacket.Measurement.class, new MeasurementTypeAdapter()) //
			.create();

	private GsonFactory() {
	}
}
