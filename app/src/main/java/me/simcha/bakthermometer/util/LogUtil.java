package me.simcha.bakthermometer.util;

import android.util.Log;

public class LogUtil {

	private static final String PREFIX = "BAK";

	private LogUtil() {
	}

	public static void i(String msg) {
		Log.i(PREFIX, msg);
	}

	public static void i(String msg, Throwable thr) {
		Log.i(PREFIX, msg, thr);
	}

	public static void e(String msg) {
		Log.e(PREFIX, msg);
	}

	public static void e(String msg, Throwable thr) {
		Log.e(PREFIX, msg, thr);
	}

	public static void d(String msg) {
		Log.d(PREFIX, msg);
	}

	public static void d(String msg, Throwable thr) {
		Log.d(PREFIX, msg, thr);
	}

	public static void v(String msg) {
		Log.v(PREFIX, msg);
	}

	public static void v(String msg, Throwable thr) {
		Log.v(PREFIX, msg, thr);
	}

	public static void w(String msg) {
		Log.w(PREFIX, msg);
	}

	public static void w(String msg, Throwable thr) {
		Log.w(PREFIX, msg, thr);
	}

	public static void wtf(String msg) {
		Log.wtf(PREFIX, msg);
	}

	public static void wtf(String msg, Throwable thr) {
		Log.wtf(PREFIX, msg, thr);
	}
}
