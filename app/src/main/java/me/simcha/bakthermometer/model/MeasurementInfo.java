package me.simcha.bakthermometer.model;

import java.util.LinkedList;
import java.util.List;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import android.os.Parcel;
import android.os.Parcelable;

import me.simcha.bakthermometer.packet.packets.serverside.NewDataPacket;

public class MeasurementInfo implements Parcelable {

	public static final Creator<MeasurementInfo> CREATOR = new Creator<MeasurementInfo>() {
		@Override
		public MeasurementInfo createFromParcel(Parcel in) {
			return new MeasurementInfo(in);
		}

		@Override
		public MeasurementInfo[] newArray(int size) {
			return new MeasurementInfo[size];
		}
	};

	private boolean running = false;
	private double desiredTemp;
	private int alarmAt;
	private List<DataPoint> measurements = new LinkedList<>();

	public MeasurementInfo() {
	}

	private MeasurementInfo(Parcel in) {
		running = in.readByte() != 0;
		desiredTemp = in.readDouble();
		alarmAt = in.readInt();

		// Read measurements
		int size = in.readInt();
		for (int i = 0; i < size; i++) {
			double time = in.readDouble();
			double temp = in.readDouble();
			addMeasurement(time, temp);
		}
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeByte((byte) (running ? 1 : 0));
		dest.writeDouble(desiredTemp);
		dest.writeInt(alarmAt);

		// Write measurements
		dest.writeInt(measurements.size());
		for (DataPoint measurement : measurements) {
			dest.writeDouble(measurement.getX());
			dest.writeDouble(measurement.getY());
		}
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void addMeasurement(NewDataPacket.Measurement measurement) {
		addMeasurement(measurement.getTimeSinceStart(), measurement.getTemperature());
	}

	public void addMeasurement(double time, double temp) {
		measurements.add(new DataPoint(time, temp));
	}

	public int getMeasurementsSize() {
		return measurements.size();
	}

	public LineGraphSeries<DataPoint> toLineGraphSeries() {
		return new LineGraphSeries<>(measurements.toArray(new DataPoint[0]));
	}

	public void clear() {
		running = false;
		desiredTemp = 0;
		alarmAt = 0;
		measurements.clear();
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public double getDesiredTemp() {
		return desiredTemp;
	}

	public void setDesiredTemp(double desiredTemp) {
		this.desiredTemp = desiredTemp;
	}

	public int getAlarmAt() {
		return alarmAt;
	}

	public void setAlarmAt(int alarmAt) {
		this.alarmAt = alarmAt;
	}
}
