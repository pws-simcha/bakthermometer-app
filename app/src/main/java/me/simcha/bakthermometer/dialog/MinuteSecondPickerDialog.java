package me.simcha.bakthermometer.dialog;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;

import me.simcha.bakthermometer.R;

public class MinuteSecondPickerDialog extends DialogFragment {

	private static final TimeSetListener NOOP_LISTENER = (m, s) -> {
	};

	private TimeSetListener onTimeSet;
	private int minMinute = 0;
	private int maxMinute = 59;
	private int minSecond = 0;
	private int maxSecond = 59;

	private NumberPicker minutesPicker;
	private NumberPicker secondsPicker;

	public MinuteSecondPickerDialog() {
		onTimeSet = NOOP_LISTENER;
	}

	@SuppressLint("ValidFragment")
	private MinuteSecondPickerDialog(TimeSetListener onTimeSet, int minMinute, int maxMinute, int minSecond,
			int maxSecond) {
		this.onTimeSet = onTimeSet;
		this.minMinute = minMinute;
		this.maxMinute = maxMinute;
		this.minSecond = minSecond;
		this.maxSecond = maxSecond;
	}

	public static void show(AppCompatActivity activity, int minMinute, int maxMinute, int minSecond, int maxSecond,
			TimeSetListener onTimeSet) {
		new MinuteSecondPickerDialog(onTimeSet, minMinute, maxMinute, minSecond, maxSecond).show(activity);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.minte_second_picker_dialog, container);

		minutesPicker = view.findViewById(R.id.minutes_picker);
		minutesPicker.setMinValue(minMinute);
		minutesPicker.setMaxValue(maxMinute);

		secondsPicker = view.findViewById(R.id.seconds_picker);
		secondsPicker.setMinValue(minSecond);
		secondsPicker.setMaxValue(maxSecond);

		view.findViewById(R.id.done_button).setOnClickListener(v -> callbackValues());

		return view;
	}

	private void callbackValues() {
		dismiss();

		int minutes = minutesPicker.getValue();
		int seconds = secondsPicker.getValue();

		onTimeSet.onTimeSet(minutes, seconds);
	}

	public void show(AppCompatActivity activity) {
		show(activity.getSupportFragmentManager(), "MinuteSecondPickerDialog");
	}

	public interface TimeSetListener {

		void onTimeSet(int minutes, int seconds);
	}
}
