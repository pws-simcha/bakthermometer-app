package me.simcha.bakthermometer.packet.packets.clientside;

import com.google.gson.annotations.SerializedName;

import me.simcha.bakthermometer.packet.Packet;

public class ChangeAlarmAtPacket extends Packet {

    @SerializedName("alarm_at")
    private int alarmAt;

    public ChangeAlarmAtPacket(int alarmAt) {
        super(Type.CHANGE_ALARM_AT);

        this.alarmAt = alarmAt;
    }
}
