package me.simcha.bakthermometer.packet.packets.clientside;

import me.simcha.bakthermometer.packet.Packet;

public class StopMeasurementPacket extends Packet {

    public StopMeasurementPacket() {
        super(Type.STOP_MEASUREMENT);
    }
}
