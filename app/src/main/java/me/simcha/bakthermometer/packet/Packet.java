package me.simcha.bakthermometer.packet;

import com.google.gson.annotations.SerializedName;

import android.content.Context;
import android.content.Intent;

import me.simcha.bakthermometer.Constants;
import me.simcha.bakthermometer.packet.packets.clientside.ChangeAlarmAtPacket;
import me.simcha.bakthermometer.packet.packets.clientside.ChangeDesiredTempPacket;
import me.simcha.bakthermometer.packet.packets.clientside.JoinMeasurementPacket;
import me.simcha.bakthermometer.packet.packets.clientside.LeaveMeasurementPacket;
import me.simcha.bakthermometer.packet.packets.clientside.StartMeasurementPacket;
import me.simcha.bakthermometer.packet.packets.clientside.StopMeasurementPacket;
import me.simcha.bakthermometer.packet.packets.serverside.AlarmPacket;
import me.simcha.bakthermometer.packet.packets.serverside.ChangedAlarmAtPacket;
import me.simcha.bakthermometer.packet.packets.serverside.ChangedDesiredTempPacket;
import me.simcha.bakthermometer.packet.packets.serverside.JoinedMeasurementPacket;
import me.simcha.bakthermometer.packet.packets.serverside.MeasurementStoppedPacket;
import me.simcha.bakthermometer.packet.packets.serverside.NewDataPacket;
import me.simcha.bakthermometer.packet.packets.serverside.NoMeasurementPacket;
import me.simcha.bakthermometer.packet.packets.serverside.StartedMeasurementPacket;
import me.simcha.bakthermometer.service.TCPService;
import me.simcha.bakthermometer.util.gson.GsonFactory;

public abstract class Packet {

	@SerializedName("id")
	private final Type type;

	protected Packet(Type type) {
		this.type = type;
	}

	public void send(Context context) {
		Intent sendPacketIntent = new Intent(context, TCPService.class);
		sendPacketIntent.setAction(Constants.SendPacket.ACTION);
		sendPacketIntent.putExtra(Constants.SendPacket.PACKET_FIELD, toJson());
		context.startService(sendPacketIntent);
	}

	public String toJson() {
		return GsonFactory.GSON.toJson(this);
	}

	public Type getType() {
		return type;
	}

	public enum Type {
		NEW_DATA(NewDataPacket.class), //
		CHANGED_DESIRED_TEMP(ChangedDesiredTempPacket.class), //
		CHANGED_ALARM_AT(ChangedAlarmAtPacket.class), //
		NO_MEASUREMENT(NoMeasurementPacket.class), //
		MEASUREMENT_STOPPED(MeasurementStoppedPacket.class), //
		JOINED_MEASUREMENT(JoinedMeasurementPacket.class), //
		STARTED_MEASUREMENT(StartedMeasurementPacket.class), //
		ALARM(AlarmPacket.class), //

		START_MEASUREMENT(StartMeasurementPacket.class), //
		CHANGE_DESIRED_TEMP(ChangeDesiredTempPacket.class), //
		CHANGE_ALARM_AT(ChangeAlarmAtPacket.class), //
		JOIN_MEASUREMENT(JoinMeasurementPacket.class), //
		STOP_MEASUREMENT(StopMeasurementPacket.class), //

		LEAVE_MEASUREMENT(LeaveMeasurementPacket.class); //

		private final Class<? extends Packet> packetClass;

		Type(Class<? extends Packet> packetClass) {
			this.packetClass = packetClass;
		}
		public Class<? extends Packet> getPacketClass() {
			return packetClass;
		}

	}
}
