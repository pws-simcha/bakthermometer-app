package me.simcha.bakthermometer.packet.packets.serverside;

import com.google.gson.annotations.SerializedName;

import me.simcha.bakthermometer.packet.Packet;

public class ChangedDesiredTempPacket extends Packet {

    @SerializedName("desired_temp")
	private double desiredTemp;

    public ChangedDesiredTempPacket() {
        super(Packet.Type.CHANGED_DESIRED_TEMP);
    }

	public double getDesiredTemp() {
        return desiredTemp;
    }
}
