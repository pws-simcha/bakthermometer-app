package me.simcha.bakthermometer.packet;

public interface IPacketListener {

	void onReceive(Packet packet);
}
