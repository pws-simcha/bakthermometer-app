package me.simcha.bakthermometer.packet.packets.serverside;

import me.simcha.bakthermometer.packet.Packet;

public class AlarmPacket extends Packet {

    public AlarmPacket() {
        super(Type.ALARM);
    }
}
