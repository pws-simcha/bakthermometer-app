package me.simcha.bakthermometer.packet.packets.clientside;

import me.simcha.bakthermometer.packet.Packet;

public class LeaveMeasurementPacket extends Packet {

	public LeaveMeasurementPacket() {
		super(Type.LEAVE_MEASUREMENT);
	}
}
