package me.simcha.bakthermometer.packet;

import com.google.gson.JsonSyntaxException;

import me.simcha.bakthermometer.util.gson.GsonFactory;

public class PacketParser {

	public Packet parsePacket(String json) {
		try {
			return GsonFactory.GSON.fromJson(json, Packet.class);
		} catch (JsonSyntaxException ex) {
			return null;
		}
	}
}
