package me.simcha.bakthermometer.packet.packets.serverside;

import me.simcha.bakthermometer.packet.Packet;

public class NoMeasurementPacket extends Packet {

    public NoMeasurementPacket() {
        super(Type.NO_MEASUREMENT);
    }
}
