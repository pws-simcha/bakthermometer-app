package me.simcha.bakthermometer.packet.packets.serverside;

import com.google.gson.annotations.SerializedName;

import me.simcha.bakthermometer.packet.Packet;

public class JoinedMeasurementPacket extends Packet {

    @SerializedName("desired_temp")
    private double desiredTemp;
    @SerializedName("alarm_at")
    private int alarmAt;

    public JoinedMeasurementPacket() {
        super(Type.JOINED_MEASUREMENT);
    }

    public double getDesiredTemp() {
        return desiredTemp;
    }

    public int getAlarmAt() {
        return alarmAt;
    }
}
