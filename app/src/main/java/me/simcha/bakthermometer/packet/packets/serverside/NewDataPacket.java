package me.simcha.bakthermometer.packet.packets.serverside;

import com.google.gson.annotations.SerializedName;

import me.simcha.bakthermometer.packet.Packet;

public class NewDataPacket extends Packet {

    private Measurement measurement;
    @SerializedName("time_until_desired_temp")
	private int timeUntilDesiredTemp;

    public NewDataPacket() {
        super(Packet.Type.NEW_DATA);
    }

    public Measurement getMeasurement() {
        return measurement;
    }

	public int getTimeUntilDesiredTemp() {
        return timeUntilDesiredTemp;
    }

    public static class Measurement {

        private double timeSinceStart;
        private double temperature;

        public Measurement(double timeSinceStart, double temperature) {
            this.timeSinceStart = timeSinceStart;
            this.temperature = temperature;
        }

        public double getTimeSinceStart() {
            return timeSinceStart;
        }

        public double getTemperature() {
            return temperature;
        }
    }
}
