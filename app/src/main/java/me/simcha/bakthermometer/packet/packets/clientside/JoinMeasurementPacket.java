package me.simcha.bakthermometer.packet.packets.clientside;

import me.simcha.bakthermometer.packet.Packet;

public class JoinMeasurementPacket extends Packet {

    public JoinMeasurementPacket() {
        super(Type.JOIN_MEASUREMENT);
    }
}
