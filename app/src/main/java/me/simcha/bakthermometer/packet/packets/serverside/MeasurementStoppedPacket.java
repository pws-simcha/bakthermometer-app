package me.simcha.bakthermometer.packet.packets.serverside;

import me.simcha.bakthermometer.packet.Packet;

public class MeasurementStoppedPacket extends Packet {

    public MeasurementStoppedPacket() {
        super(Type.MEASUREMENT_STOPPED);
    }
}
