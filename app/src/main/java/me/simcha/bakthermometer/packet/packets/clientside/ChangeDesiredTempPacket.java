package me.simcha.bakthermometer.packet.packets.clientside;

import com.google.gson.annotations.SerializedName;

import me.simcha.bakthermometer.packet.Packet;

public class ChangeDesiredTempPacket extends Packet {

    @SerializedName("desired_temp")
    private double desiredTemp;

    public ChangeDesiredTempPacket(double desiredTemp) {
        super(Type.CHANGE_DESIRED_TEMP);

        this.desiredTemp = desiredTemp;
    }
}
