package me.simcha.bakthermometer.packet.packets.clientside;

import com.google.gson.annotations.SerializedName;

import me.simcha.bakthermometer.packet.Packet;

public class StartMeasurementPacket extends Packet {

    @SerializedName("desired_temp")
    private double desiredTemp;
    @SerializedName("alarm_at")
    private int alarmAt;

    public StartMeasurementPacket(double desiredTemp, int alarmAt) {
        super(Type.START_MEASUREMENT);

        this.desiredTemp = desiredTemp;
        this.alarmAt = alarmAt;
    }
}
