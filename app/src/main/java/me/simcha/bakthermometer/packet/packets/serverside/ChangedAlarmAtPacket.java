package me.simcha.bakthermometer.packet.packets.serverside;

import com.google.gson.annotations.SerializedName;

import me.simcha.bakthermometer.packet.Packet;

public class ChangedAlarmAtPacket extends Packet {

    @SerializedName("alarm_at")
	private int alarmAt;

    public ChangedAlarmAtPacket() {
        super(Type.CHANGED_ALARM_AT);
    }

	public int getAlarmAt() {
        return alarmAt;
    }
}
