package me.simcha.bakthermometer.activity;

import java.util.concurrent.TimeUnit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import me.simcha.bakthermometer.Constants;
import me.simcha.bakthermometer.R;
import me.simcha.bakthermometer.broadcast.receiver.ExceptionBroadcastReceiver;
import me.simcha.bakthermometer.broadcast.receiver.PacketBroadcastReceiver;
import me.simcha.bakthermometer.dialog.MinuteSecondPickerDialog;
import me.simcha.bakthermometer.model.MeasurementInfo;
import me.simcha.bakthermometer.packet.IPacketListener;
import me.simcha.bakthermometer.packet.Packet;
import me.simcha.bakthermometer.packet.packets.clientside.StartMeasurementPacket;
import me.simcha.bakthermometer.packet.packets.serverside.StartedMeasurementPacket;
import me.simcha.bakthermometer.util.LogUtil;

public class StartActivity extends AppCompatActivity {

	private PacketBroadcastReceiver packetBroadcastReceiver;
	private ExceptionBroadcastReceiver exceptionBroadcastReceiver;

	private int alarmAt = -1;
	private double temperature = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_start);

		packetBroadcastReceiver = new PacketBroadcastReceiver(new PacketListener());
		packetBroadcastReceiver.register(this);
		exceptionBroadcastReceiver = new ExceptionBroadcastReceiver(new ExceptionListener());
		exceptionBroadcastReceiver.register(this);

		EditText timeDisplay = findViewById(R.id.timeDisplay);
		Button startButton = findViewById(R.id.startMeasurementButton);
		startButton.setOnClickListener(v -> {
			if (alarmAt < 0 || temperature < 0) {
				Toast.makeText(this, getString(R.string.cant_start_yet), Toast.LENGTH_SHORT).show();
			} else {
				new StartMeasurementPacket(temperature, alarmAt).send(this);
				startButton.setClickable(false); // Prevent sending multiple packets
			}
		});

		findViewById(R.id.timeButton)
				.setOnClickListener(view -> MinuteSecondPickerDialog.show(this, 0, 20, 0, 59, (m, s) -> {
					// We can cast it since we know the max minute is 20
					int minutesInMillis = (int) TimeUnit.MINUTES.toMillis(m);
					int secondsInMillis = (int) TimeUnit.SECONDS.toMillis(s);

					alarmAt = minutesInMillis + secondsInMillis;

					timeDisplay.setText(getString(R.string.formatted_time, m, s));
				}));

		EditText editTextTemperature = findViewById(R.id.editTextTemperature);
		editTextTemperature.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				try {
					temperature = Double.parseDouble(s.toString());
				} catch (NumberFormatException ex) {
					temperature = -1;
				}
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO: Double check or something
		Toast.makeText(this, "Can't quit", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		unregisterReceiver(packetBroadcastReceiver);
		unregisterReceiver(exceptionBroadcastReceiver);
	}

	private class ExceptionListener implements ExceptionBroadcastReceiver.IExceptionListener {

		@Override
		public void onReceive(Exception ex) {
			// TODO: Handle exceptions better
			LogUtil.e(ex.toString(), ex);
		}
	}

	private class PacketListener implements IPacketListener {

		@Override
		public void onReceive(Packet packet) {
			if (packet.getType() == Packet.Type.STARTED_MEASUREMENT) {
				StartedMeasurementPacket smPacket = (StartedMeasurementPacket) packet;

				Intent intent = new Intent(StartActivity.this, MeasurementActivity.class);

				MeasurementInfo info = new MeasurementInfo();
				info.setRunning(true);
				info.setAlarmAt(smPacket.getAlarmAt());
				info.setDesiredTemp(smPacket.getDesiredTemp());
				intent.putExtra(Constants.MeasurementActivityIntent.INFO_FIELD, info);

				startActivity(intent);
				finish();
			}
		}
	}
}
