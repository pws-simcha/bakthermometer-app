package me.simcha.bakthermometer.activity;

import java.io.IOException;

import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.WindowManager;

import me.simcha.bakthermometer.R;
import me.simcha.bakthermometer.util.LogUtil;

public class AlarmActivity extends FullscreenActivity {

	private static final long[] VIBRATE_PATTERN = new long[]{500, 500};

	private Vibrator vibrator;
	private AudioManager audioManager;
	private MediaPlayer mediaPlayer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_alarm);

		findViewById(R.id.disableAlarmButton).setOnClickListener(v -> stopAlarm());

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
				| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
				| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

		vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
		vibrate();

		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnErrorListener((mp, what, extra) -> {
			LogUtil.e("Error occurred while playing audio.");
			mp.stop();
			mp.release();
			mediaPlayer = null;
			return true;
		});

		try {
			mediaPlayer.setDataSource(this, getAlert());
			startAlarm(mediaPlayer);
		} catch (Exception ex) {
			LogUtil.e("Failed to play ringtone", ex);
		}
	}

	private void startAlarm(MediaPlayer mediaPlayer)
			throws IOException, IllegalArgumentException, IllegalStateException {
		if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
			mediaPlayer.setLooping(true);
			mediaPlayer.prepare();
			mediaPlayer.start();
		}
	}

	private void vibrate() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			vibrator.vibrate(VibrationEffect.createWaveform(VIBRATE_PATTERN, 0),
					new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_ALARM).build());
		} else {
			vibrator.vibrate(VIBRATE_PATTERN, 0);
		}
	}

	private void stopAlarm() {
		vibrator.cancel();
		mediaPlayer.stop();

		startActivity(new Intent(this, SplashScreenActivity.class));

		finish();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			stopAlarm();
			return true;
		}

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			stopAlarm();
		}

		return super.onKeyDown(keyCode, event);
	}

	private Uri getAlert() {
		Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

		if (alert == null) {
			// alert is null, using backup
			alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

			// Last last backup
			if (alert == null) {
				alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
			}
		}

		return alert;
	}
}
