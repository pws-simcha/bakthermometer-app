package me.simcha.bakthermometer.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import me.simcha.bakthermometer.Constants;
import me.simcha.bakthermometer.R;

public class NoServerActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_no_server);

		SharedPreferences sharedPreferences = getSharedPreferences(Constants.SharedPreferences.FILE_NAME, MODE_PRIVATE);
		String ip = sharedPreferences.getString(Constants.SharedPreferences.IP_FIELD, "localhost");
		int port = sharedPreferences.getInt(Constants.SharedPreferences.PORT_FIELD, 8192);
		((TextView) findViewById(R.id.ipTextView)).setText(ip);
		((TextView) findViewById(R.id.portTextView)).setText(Integer.toString(port));

		EditText ipEditText = findViewById(R.id.ipEditText);
		findViewById(R.id.updateIpButton).setOnClickListener(v -> {
			sharedPreferences.edit() //
					.putString(Constants.SharedPreferences.IP_FIELD, ipEditText.getText().toString()) //
					.apply();
			Toast.makeText(this, getString(R.string.updated_ip), Toast.LENGTH_SHORT).show();
		});

		EditText portEditText = findViewById(R.id.portEditText);
		findViewById(R.id.updatePortButton).setOnClickListener(v -> {
			sharedPreferences.edit() //
					.putInt(Constants.SharedPreferences.PORT_FIELD, Integer.valueOf(portEditText.getText().toString())) //
					.apply();
			Toast.makeText(this, getString(R.string.updated_port), Toast.LENGTH_SHORT).show();
		});

		findViewById(R.id.closeAppButton).setOnClickListener(v -> finishAffinity());
	}
}
