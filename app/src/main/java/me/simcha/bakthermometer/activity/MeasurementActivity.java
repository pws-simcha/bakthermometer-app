package me.simcha.bakthermometer.activity;

import java.util.concurrent.TimeUnit;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import me.simcha.bakthermometer.Constants;
import me.simcha.bakthermometer.R;
import me.simcha.bakthermometer.broadcast.receiver.ExceptionBroadcastReceiver;
import me.simcha.bakthermometer.broadcast.receiver.PacketBroadcastReceiver;
import me.simcha.bakthermometer.dialog.MinuteSecondPickerDialog;
import me.simcha.bakthermometer.model.MeasurementInfo;
import me.simcha.bakthermometer.packet.IPacketListener;
import me.simcha.bakthermometer.packet.Packet;
import me.simcha.bakthermometer.packet.packets.clientside.ChangeAlarmAtPacket;
import me.simcha.bakthermometer.packet.packets.clientside.ChangeDesiredTempPacket;
import me.simcha.bakthermometer.packet.packets.clientside.LeaveMeasurementPacket;
import me.simcha.bakthermometer.packet.packets.clientside.StopMeasurementPacket;
import me.simcha.bakthermometer.packet.packets.serverside.ChangedAlarmAtPacket;
import me.simcha.bakthermometer.packet.packets.serverside.ChangedDesiredTempPacket;
import me.simcha.bakthermometer.packet.packets.serverside.NewDataPacket;
import me.simcha.bakthermometer.service.TCPService;
import me.simcha.bakthermometer.util.LogUtil;

public class MeasurementActivity extends AppCompatActivity {

	private PacketBroadcastReceiver packetBroadcastReceiver;
	private ExceptionBroadcastReceiver exceptionBroadcastReceiver;

	private Viewport viewport;
	private LineGraphSeries<DataPoint> measurementSeries;
	private LineGraphSeries<DataPoint> desiredTempSeries;
	private double maxX = 3600;

	private TextView currentTempTextView;
	private TextView timeToDesiredTempTextView;
	private TextView timeToDesiredTempValueTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_measurement);

		packetBroadcastReceiver = new PacketBroadcastReceiver(new PacketListener());
		packetBroadcastReceiver.register(this);
		exceptionBroadcastReceiver = new ExceptionBroadcastReceiver(new ExceptionListener());
		exceptionBroadcastReceiver.register(this);

		if (!getIntent().hasExtra(Constants.MeasurementActivityIntent.INFO_FIELD)) {
			Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
			finishAffinity();
			return;
		}

		MeasurementInfo info = getIntent().getParcelableExtra(Constants.MeasurementActivityIntent.INFO_FIELD);

		initGraph(info);

		currentTempTextView = findViewById(R.id.currentTempTextView);
		timeToDesiredTempValueTextView = findViewById(R.id.timeToDesiredTempValueTextView);
		timeToDesiredTempTextView = findViewById(R.id.timeToDesiredTempTextView);
		updateTimeToDesiredTempTextView(info.getDesiredTemp());

		initButtons();
	}

	private void initGraph(MeasurementInfo info) {
		GraphView graph = findViewById(R.id.graph);

		desiredTempSeries = new LineGraphSeries<>(new DataPoint[]{ //
				new DataPoint(0, info.getDesiredTemp()), //
				new DataPoint(maxX, info.getDesiredTemp()) //
		});
		desiredTempSeries.setColor(Color.RED);
		graph.addSeries(desiredTempSeries);

		if (info.getMeasurementsSize() > 0) {
			measurementSeries = info.toLineGraphSeries();
		} else {
			measurementSeries = new LineGraphSeries<>();
		}
		graph.addSeries(measurementSeries);

		viewport = graph.getViewport();
		viewport.setXAxisBoundsManual(true);
		viewport.setYAxisBoundsManual(true);
		viewport.setMinY(15);
		viewport.setMaxY(info.getDesiredTemp() + 5);
		viewport.setMinX(0);
		viewport.setMaxX(maxX);

		viewport.setScalable(true);
		viewport.setScalableY(true);
	}

	private void initButtons() {
		findViewById(R.id.changeTempButton).setOnClickListener(v -> {
			NumberPicker numberPicker = new NumberPicker(this);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.MATCH_PARENT);
			numberPicker.setLayoutParams(lp);
			numberPicker.setMinValue(0);
			numberPicker.setMaxValue(150);

			new AlertDialog.Builder(this) //
					.setView(numberPicker) //
					.setTitle(R.string.change_temp) //
					.setPositiveButton(R.string.change,
							(dialog, which) -> new ChangeDesiredTempPacket(numberPicker.getValue()).send(this)) //
					.setNegativeButton(android.R.string.no, null) //
					.show();
		});

		findViewById(R.id.changeAlarmButton).setOnClickListener(v -> {
			MinuteSecondPickerDialog.show(this, 0, 20, 0, 59, (m, s) -> {
				// We can cast it since we know the max minute is 20
				int minutesInMillis = (int) TimeUnit.MINUTES.toMillis(m);
				int secondsInMillis = (int) TimeUnit.SECONDS.toMillis(s);

				int alarmAt = minutesInMillis + secondsInMillis;
				new ChangeAlarmAtPacket(alarmAt).send(this);
			});
		});

		findViewById(R.id.stopMeasurementButton).setOnClickListener(v -> {
			showConfirmDialog( //
					getString(R.string.are_you_sure), //
					getString(R.string.confirm_stop_measurement), //
					(dialog, which) -> {
						new StopMeasurementPacket().send(this);
						finishAffinity();
					} //
			);
		});

		findViewById(R.id.leaveMeasurementButton).setOnClickListener(v -> {
			showConfirmDialog( //
					getString(R.string.are_you_sure), //
					getString(R.string.confirm_leave_measurement), //
					(dialog, which) -> {
						new LeaveMeasurementPacket().send(this);
						stopService(new Intent(this, TCPService.class));
						finishAffinity();
					} //
			);
		});
	}

	private void showConfirmDialog(String title, String message, DialogInterface.OnClickListener onConfirm) {
		new AlertDialog.Builder(this) //
				.setTitle(title) //
				.setMessage(message) //
				.setIcon(android.R.drawable.ic_dialog_alert) //
				.setPositiveButton(android.R.string.yes, onConfirm) //
				.setNegativeButton(android.R.string.no, null) //
				.show();
	}

	private void updateTimeToDesiredTempTextView(double desiredTemp) {
		timeToDesiredTempTextView.setText(getString(R.string.time_to_desired_temp, desiredTemp));
	}

	@Override
	public void onBackPressed() {
		// TODO: Double check or something
		Toast.makeText(this, "Can't quit", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		unregisterReceiver(packetBroadcastReceiver);
		unregisterReceiver(exceptionBroadcastReceiver);
	}

	private class ExceptionListener implements ExceptionBroadcastReceiver.IExceptionListener {

		@Override
		public void onReceive(Exception ex) {
			// TODO: Handle exceptions better
			LogUtil.e(ex.toString(), ex);
		}
	}

	private void showInfoDialog(String message) {
		showInfoDialog(message, (dialog, which) -> {
		});
	}

	private void showInfoDialog(String message, DialogInterface.OnClickListener onClose) {
		new AlertDialog.Builder(this) //
				.setMessage(message) //
				.setPositiveButton("Okay", onClose) //
				.create() //
				.show();
	}

	private class PacketListener implements IPacketListener {

		@Override
		public void onReceive(Packet packet) {
			switch (packet.getType()) {
				case NEW_DATA :
					onReceivedNewData((NewDataPacket) packet);
					break;
				case CHANGED_DESIRED_TEMP :
					onChangedDesiredTemp((ChangedDesiredTempPacket) packet);
					break;
				case CHANGED_ALARM_AT :
					onChangedAlarmAt((ChangedAlarmAtPacket) packet);
					break;
				case MEASUREMENT_STOPPED :
					onMeasurementStopped();
					break;
			}
		}

		private void onReceivedNewData(NewDataPacket packet) {
			measurementSeries.appendData( //
					new DataPoint( //
							packet.getMeasurement().getTimeSinceStart(), //
							packet.getMeasurement().getTemperature() //
					), //
					false, Integer.MAX_VALUE //
			);

			if (packet.getMeasurement().getTimeSinceStart() > maxX) {
				maxX += 600;
				desiredTempSeries.appendData(new DataPoint(maxX, desiredTempSeries.getHighestValueY()), true,
						Integer.MAX_VALUE);
				viewport.setMaxX(maxX);
			}

			currentTempTextView.setText(getString(R.string.formatted_temp, packet.getMeasurement().getTemperature()));

			int timeUntilDesiredTemp = packet.getTimeUntilDesiredTemp();
			if (timeUntilDesiredTemp > 0) {
				long m = TimeUnit.MILLISECONDS.toMinutes(timeUntilDesiredTemp);
				long remainingMillis = timeUntilDesiredTemp - TimeUnit.MINUTES.toMillis(m);
				long s = TimeUnit.MILLISECONDS.toSeconds(remainingMillis);

				timeToDesiredTempValueTextView.setText(getString(R.string.formatted_time, m, s));
			}
		}

		private void onChangedDesiredTemp(ChangedDesiredTempPacket packet) {
			desiredTempSeries.resetData(new DataPoint[]{ //
					new DataPoint(0, packet.getDesiredTemp()), //
					new DataPoint(maxX, packet.getDesiredTemp()) //
			});

			updateTimeToDesiredTempTextView(packet.getDesiredTemp());
			showInfoDialog(getString(R.string.desired_temp_changed_to, packet.getDesiredTemp()));
		}

		private void onChangedAlarmAt(ChangedAlarmAtPacket packet) {
			long m = TimeUnit.MILLISECONDS.toMinutes(packet.getAlarmAt());
			long remainingMillis = packet.getAlarmAt() - TimeUnit.MINUTES.toMillis(m);
			long s = TimeUnit.MILLISECONDS.toSeconds(remainingMillis);

			showInfoDialog(getString(R.string.alarm_changed_to, m, s));
		}

		private void onMeasurementStopped() {
			showInfoDialog(getString(R.string.measurement_stopped), (dialog, which) -> finishAffinity());
		}
	}
}
