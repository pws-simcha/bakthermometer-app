package me.simcha.bakthermometer.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

public abstract class FullscreenActivity extends Activity {

	private Handler handler;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		handler = new Handler();

		setFullscreen();
		registerSystemUiVisibility();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterSystemUiVisibility();
		exitFullscreen();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus) {
			handler.removeCallbacks(this::setFullscreen);
			handler.postDelayed(this::setFullscreen, 300);
		} else {
			handler.removeCallbacks(this::setFullscreen);
		}
	}

	@Override
	public void onStop() {
		handler.removeCallbacks(this::setFullscreen);
		super.onStop();
	}

	private void setFullscreen() {
		int flags = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_FULLSCREEN;

		flags |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

		getWindow().getDecorView().setSystemUiVisibility(flags);
	}

	private void exitFullscreen() {
		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
	}

	private void registerSystemUiVisibility() {
		getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(visibility -> {
			if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
				setFullscreen();
			}
		});
	}

	private void unregisterSystemUiVisibility() {
		getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(null);
	}
}
