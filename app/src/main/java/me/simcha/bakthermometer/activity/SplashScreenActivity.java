package me.simcha.bakthermometer.activity;

import java.net.ConnectException;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.util.Consumer;
import android.support.v7.app.AppCompatActivity;

import me.simcha.bakthermometer.Constants;
import me.simcha.bakthermometer.R;
import me.simcha.bakthermometer.broadcast.receiver.ExceptionBroadcastReceiver;
import me.simcha.bakthermometer.broadcast.receiver.MeasurementInfoBroadcastReceiver;
import me.simcha.bakthermometer.broadcast.receiver.PacketBroadcastReceiver;
import me.simcha.bakthermometer.model.MeasurementInfo;
import me.simcha.bakthermometer.packet.IPacketListener;
import me.simcha.bakthermometer.packet.Packet;
import me.simcha.bakthermometer.packet.packets.clientside.JoinMeasurementPacket;
import me.simcha.bakthermometer.packet.packets.serverside.JoinedMeasurementPacket;
import me.simcha.bakthermometer.service.TCPService;
import me.simcha.bakthermometer.util.LogUtil;

public class SplashScreenActivity extends AppCompatActivity {

	private PacketBroadcastReceiver packetBroadcastReceiver;
	private ExceptionBroadcastReceiver exceptionBroadcastReceiver;
	private MeasurementInfoBroadcastReceiver measurementInfoBroadcastReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		createNotificationChannel();

		packetBroadcastReceiver = new PacketBroadcastReceiver(new PacketListener());
		packetBroadcastReceiver.register(this);
		exceptionBroadcastReceiver = new ExceptionBroadcastReceiver(new ExceptionListener());
		exceptionBroadcastReceiver.register(this);
		measurementInfoBroadcastReceiver = new MeasurementInfoBroadcastReceiver(info -> {
			if (info.isRunning()) {
				startNewActivity(MeasurementActivity.class,
						intent -> intent.putExtra(Constants.MeasurementActivityIntent.INFO_FIELD, info));
			} else {
				new JoinMeasurementPacket().send(this);
			}
		});
		measurementInfoBroadcastReceiver.register(this);

		SharedPreferences sharedPreferences = getSharedPreferences(Constants.SharedPreferences.FILE_NAME, MODE_PRIVATE);
		String ip = sharedPreferences.getString(Constants.SharedPreferences.IP_FIELD, "localhost");
		int port = sharedPreferences.getInt(Constants.SharedPreferences.PORT_FIELD, 8192);

		Intent startServiceIntent = new Intent(this, TCPService.class);
		startServiceIntent.setAction(Constants.StartTCPService.ACTION);
		startServiceIntent.putExtra(Constants.StartTCPService.IP_FIELD, ip);
		startServiceIntent.putExtra(Constants.StartTCPService.PORT_FIELD, port);
		startService(startServiceIntent);

		Intent getMeasurementInfoIntent = new Intent(this, TCPService.class);
		getMeasurementInfoIntent.setAction(Constants.GetMeasurementInfo.ACTION);
		startService(getMeasurementInfoIntent);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		unregisterReceiver(packetBroadcastReceiver);
		unregisterReceiver(exceptionBroadcastReceiver);
		unregisterReceiver(measurementInfoBroadcastReceiver);
	}

	private void startNewActivity(Class<? extends Activity> activityClass) {
		startNewActivity(activityClass, intent -> {
		});
	}

	private void startNewActivity(Class<? extends Activity> activityClass, Consumer<Intent> consumer) {
		Intent intent = new Intent(this, activityClass);
		consumer.accept(intent);
		startActivity(intent);
		finish();
	}

	private void createNotificationChannel() {
		// Create the NotificationChannel, but only on API 26+ because
		// the NotificationChannel class is new and not in the support library
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			CharSequence name = getString(R.string.notification_channel_name);
			String description = getString(R.string.notification_channel_description);
			int importance = NotificationManager.IMPORTANCE_DEFAULT;
			NotificationChannel channel = new NotificationChannel(Constants.Notification.CHANNEL_ID, name, importance);
			channel.setDescription(description);
			// Register the channel with the system; you can't change the importance
			// or other notification behaviors after this
			NotificationManager notificationManager = getSystemService(NotificationManager.class);
			notificationManager.createNotificationChannel(channel);
		}
	}

	private class ExceptionListener implements ExceptionBroadcastReceiver.IExceptionListener {

		@Override
		public void onReceive(Exception ex) {
			if (ex instanceof ConnectException) {
				startNewActivity(NoServerActivity.class);
				stopService(new Intent(SplashScreenActivity.this, TCPService.class));
			} else {
				LogUtil.e(ex.toString(), ex);
				// TODO: Handle other exceptions
			}
		}
	}

	private class PacketListener implements IPacketListener {

		@Override
		public void onReceive(Packet packet) {
			// Should be the only two packets we can receive here
			if (packet.getType() == Packet.Type.NO_MEASUREMENT) {
				startNewActivity(StartActivity.class);
			} else if (packet.getType() == Packet.Type.JOINED_MEASUREMENT) {
				startNewActivity(MeasurementActivity.class, intent -> {
					JoinedMeasurementPacket joinedMeasurementPacket = (JoinedMeasurementPacket) packet;

					MeasurementInfo info = new MeasurementInfo();
					info.setRunning(true);
					info.setAlarmAt(joinedMeasurementPacket.getAlarmAt());
					info.setDesiredTemp(joinedMeasurementPacket.getDesiredTemp());
					intent.putExtra(Constants.MeasurementActivityIntent.INFO_FIELD, info);
				});
			}
		}
	}
}
