package me.simcha.bakthermometer.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;

import me.simcha.bakthermometer.Constants;
import me.simcha.bakthermometer.R;
import me.simcha.bakthermometer.activity.AlarmActivity;
import me.simcha.bakthermometer.activity.SplashScreenActivity;
import me.simcha.bakthermometer.model.MeasurementInfo;
import me.simcha.bakthermometer.packet.Packet;
import me.simcha.bakthermometer.packet.PacketParser;
import me.simcha.bakthermometer.packet.packets.serverside.ChangedAlarmAtPacket;
import me.simcha.bakthermometer.packet.packets.serverside.ChangedDesiredTempPacket;
import me.simcha.bakthermometer.packet.packets.serverside.JoinedMeasurementPacket;
import me.simcha.bakthermometer.packet.packets.serverside.NewDataPacket;
import me.simcha.bakthermometer.packet.packets.serverside.StartedMeasurementPacket;
import me.simcha.bakthermometer.util.LogUtil;

public class TCPService extends Service {

	private NotificationManagerCompat notificationManager;
	private Notification.Builder notificationBuilder;

	private ExecutorService sendPacketThreadPool;
	private ExecutorService serverConnectionThreadPool;

	private Socket socket;
	private PrintWriter serverOutputPrintWriter;
	private BufferedReader serverInputBufferedReader;

	private boolean running = false;

	private MeasurementInfo measurementInfo;

	private PacketParser packetParser;

	@Override
	public void onCreate() {
		Intent notificationIntent = new Intent(this, SplashScreenActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

		notificationManager = NotificationManagerCompat.from(this);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			notificationBuilder = new Notification.Builder(this, Constants.Notification.CHANNEL_ID);
		} else {
			notificationBuilder = new Notification.Builder(this);
		}
		notificationBuilder //
				.setContentTitle(getString(R.string.notification_channel_name)) //
				.setContentText(getString(R.string.waiting_for_connection)) //
				.setSmallIcon(R.drawable.oven) //
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
				.setContentIntent(pendingIntent) //
				.setOngoing(true);

		sendPacketThreadPool = Executors.newCachedThreadPool();
		serverConnectionThreadPool = Executors.newSingleThreadExecutor();

		packetParser = new PacketParser();

		measurementInfo = new MeasurementInfo();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent == null || intent.getAction() == null) {
			return START_STICKY;
		}

		if (!running && intent.getAction().equals(Constants.StartTCPService.ACTION)) {
			running = true;
			LogUtil.i("Starting service");
			String ip = intent.getStringExtra(Constants.StartTCPService.IP_FIELD);
			int port = intent.getIntExtra(Constants.StartTCPService.PORT_FIELD, 8192);
			onStart(ip, port);
		}

		if (intent.getAction().equals(Constants.SendPacket.ACTION)) {
			sendPacket(intent);
		}

		if (intent.getAction().equals(Constants.GetMeasurementInfo.ACTION)) {
			broadcastMeasurementInfo();
		}

		return START_STICKY;
	}

	private void sendPacket(Intent intent) {
		String packetJson = intent.getStringExtra(Constants.SendPacket.PACKET_FIELD);
		LogUtil.i("Sending " + packetJson);

		sendPacketThreadPool.submit(() -> serverOutputPrintWriter.println(packetJson));
	}

	private void onStart(String ip, int port) {
		startForeground(Constants.Notification.ONGOING_NOTIFICATION_ID, notificationBuilder.build());

		serverConnectionThreadPool.submit(() -> {
			try {
				socket = new Socket(ip, port);

				serverOutputPrintWriter = new PrintWriter(socket.getOutputStream(), true);
				serverInputBufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

				String receivedData;
				while ((running && !Thread.currentThread().isInterrupted())
						&& ((receivedData = serverInputBufferedReader.readLine()) != null)) {
					broadcastPacket(receivedData);
					handlePacket(receivedData);
				}
			} catch (Exception ex) {
				broadcastException(ex);
			}
		});
	}

	private void handlePacket(String receivedData) {
		LogUtil.i("Received: " + receivedData);

		Packet packet = packetParser.parsePacket(receivedData);
		if (packet == null) {
			// Received data isn't a valid packet. Ignore it.
			return;
		}

		switch (packet.getType()) {
			case NEW_DATA :
				NewDataPacket newDataPacket = (NewDataPacket) packet;
				measurementInfo.addMeasurement(newDataPacket.getMeasurement());

				int timeUntilDesiredTemp = newDataPacket.getTimeUntilDesiredTemp();
				if (timeUntilDesiredTemp > 0) {
					long m = TimeUnit.MILLISECONDS.toMinutes(timeUntilDesiredTemp);
					long remainingMillis = timeUntilDesiredTemp - TimeUnit.MINUTES.toMillis(m);
					long s = TimeUnit.MILLISECONDS.toSeconds(remainingMillis);

					updateNotification(getString(R.string.notification_message,
							newDataPacket.getMeasurement().getTemperature(), m + "m " + s + "s"));
				} else {
					updateNotification(getString(R.string.notification_message,
							newDataPacket.getMeasurement().getTemperature(), getString(R.string.not_available)));
				}
				break;
			case CHANGED_DESIRED_TEMP :
				measurementInfo.setDesiredTemp(((ChangedDesiredTempPacket) packet).getDesiredTemp());
				break;
			case CHANGED_ALARM_AT :
				measurementInfo.setAlarmAt(((ChangedAlarmAtPacket) packet).getAlarmAt());
				break;
			case MEASUREMENT_STOPPED :
				stopSelf();
				break;
			case ALARM :
				Intent startAlarmIntent = new Intent(this, AlarmActivity.class);
				startAlarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(startAlarmIntent);
				break;
			case STARTED_MEASUREMENT :
				StartedMeasurementPacket smPacket = (StartedMeasurementPacket) packet;
				initializeMeasurementInfo(smPacket.getDesiredTemp(), smPacket.getAlarmAt());
				break;
			case JOINED_MEASUREMENT :
				JoinedMeasurementPacket jmPacket = (JoinedMeasurementPacket) packet;
				initializeMeasurementInfo(jmPacket.getDesiredTemp(), jmPacket.getAlarmAt());
				break;

			// We don't handle clientside packets and other serverside packets here
			default :
				break;
		}
	}

	private void initializeMeasurementInfo(double desiredTemp, int alarmAt) {
		measurementInfo.clear();

		measurementInfo.setRunning(true);
		measurementInfo.setDesiredTemp(desiredTemp);
		measurementInfo.setAlarmAt(alarmAt);
	}

	private void updateNotification(String message) {
		notificationBuilder //
				.setContentText(message) //
				.setStyle(new Notification.BigTextStyle().bigText(message));
		notificationManager.notify(Constants.Notification.ONGOING_NOTIFICATION_ID, notificationBuilder.build());
	}

	@Override
	public void onDestroy() {
		running = false;

		if (socket != null) {
			try {
				socket.close();
			} catch (IOException ex) {
				broadcastException(ex);
			}
		}

		sendPacketThreadPool.shutdown();
		serverConnectionThreadPool.shutdown();

		LogUtil.i("Stopped service");
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	private void broadcastMeasurementInfo() {
		Intent intent = new Intent();
		intent.setAction(Constants.MeasurementInfoBroadcast.ACTION);
		intent.putExtra(Constants.MeasurementInfoBroadcast.INFO_FIELD, measurementInfo);
		sendBroadcast(intent);
	}

	private void broadcastPacket(String packetJson) {
		Intent intent = new Intent();
		intent.setAction(Constants.PacketBroadcast.ACTION);
		intent.putExtra(Constants.PacketBroadcast.PACKET_FIELD, packetJson);
		sendBroadcast(intent);
	}

	private void broadcastException(Exception ex) {
		Intent intent = new Intent();
		intent.setAction(Constants.ExceptionBroadcast.ACTION);
		intent.putExtra(Constants.ExceptionBroadcast.EXCEPTION_FIELD, ex);
		sendBroadcast(intent);
	}
}
