package me.simcha.bakthermometer;

public class Constants {

	private static final String PREFIX = "me.simcha.bakthermometer.";

	public static class SharedPreferences {

		public static final String FILE_NAME = "me.simcha.bakthermometer";
		public static final String IP_FIELD = PREFIX + "ip";
		public static final String PORT_FIELD = PREFIX + "port";
	}

	public static class Notification {

		public static final String CHANNEL_ID = "BAKTHERMOMETER";
		public static final int ONGOING_NOTIFICATION_ID = 1337;
	}

	public static class StartTCPService {

		public static final String ACTION = PREFIX + "StartTCPService";
		public static final String IP_FIELD = "ip";
		public static final String PORT_FIELD = "port";
	}

	public static class GetMeasurementInfo {

		public static final String ACTION = PREFIX + "GetMeasurementInfo";
	}

	public static class MeasurementActivityIntent {

		public static final String INFO_FIELD = "info";
	}

	public static class MeasurementInfoBroadcast {

		public static final String ACTION = PREFIX + "MeasurementInfoBroadcast";
		public static final String INFO_FIELD = "info";
	}

	public static class PacketBroadcast {

		public static final String ACTION = PREFIX + "PacketReceived";
		public static final String PACKET_FIELD = "packet";
	}

	public static class ExceptionBroadcast {

		public static final String ACTION = PREFIX + "Exception";
		public static final String EXCEPTION_FIELD = "exception";
	}

	public static class SendPacket {

		public static final String ACTION = PREFIX + "SendPacket";
		public static final String PACKET_FIELD = "packet";
	}
}
